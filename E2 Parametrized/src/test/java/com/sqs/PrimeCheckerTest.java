package com.sqs;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class PrimeCheckerTest {

	@Parameterized.Parameters
	public static Collection<Object[]> generateTestData() {
		return Arrays.asList(new Object[][] {
		         { 1, false },
		         { 2, true },
             { 3, true },
             { 4, false },
		         { 15, false },
             { 16, false },
		         { 37, true },
		         { 41, true }
		      });
	}

	private Boolean result;
	private Integer number;
	
	public PrimeCheckerTest(Integer aNumber, Boolean aResult) {
		number = aNumber;
		result = aResult;
	}
	
	@Test
	public void isPrime() {
		assertEquals(String.format("result of isPrime(%d) is ", number, result), result, PrimeChecker.isPrime(number));
	}
}
