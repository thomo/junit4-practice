package fft;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private String start;
    private String output;
    private String filter;

    public Main(String[] args) {
        start = args[0];
        filter = args[1];
        output = args[2];
    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) {
        Main app = new Main(args);

        try {
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void run() throws IOException {
        File s = new File(start);
        List<String> all = new ArrayList<String>();
        all.add(s.getCanonicalPath());
        all.addAll(Arrays.asList(loopA(s)));

        BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(filter))));
        String strLine;
        while ((strLine = br.readLine()) != null) {
            Pattern p = Pattern.compile(strLine);

            List<String> r = new ArrayList<String>();
            for (String fs : all) {
                Matcher m = p.matcher(fs);
                if (m.find()) {
                    r.add(fs);
                }
            }
            all.removeAll(r); 
        }
        br.close();

        BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        for (String fs : all) {
            bw.write(fs);
        }
        bw.close();
    }

    private String[] loopA(File s) throws IOException {
    	List<String> arr = new ArrayList<String>();
        if (s.isDirectory()) {
            File[] a = s.listFiles();
            for (File f : a) {
                if (f.isDirectory()) {
                    arr.addAll(Arrays.asList(loopA(f)));
                } else {
                    arr.add(f.getCanonicalPath());
                }
            }
        }
    	return arr.toArray(new String[0]);
    }
}
