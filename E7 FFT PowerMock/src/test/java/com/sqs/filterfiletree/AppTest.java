package com.sqs.filterfiletree;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(App.class)
public class AppTest {

	@Test
	public void mockStaticSystemMethod() {
		App app = new App(new String[] {"a", "b", "c"});
		
		PowerMockito.mockStatic(Pattern.class);
		Mockito.when(Pattern.compile(Mockito.anyString())).thenThrow(new RuntimeException("foo"));
		
		try {
			app.createPattern("a");
			fail("no exception thrown");
		} catch (RuntimeException e) {
			// pass
		} 
	}
	
	@Test
	public void hamcrest_StartsWith() {
		String a = "Hello World";
        assertThat(a, startsWith("Hello"));
	}
	
	@Test
	public void hamcrest_Is() {
		int x = 3;
		assertThat(x, is(equalTo(3)));
	}
}


