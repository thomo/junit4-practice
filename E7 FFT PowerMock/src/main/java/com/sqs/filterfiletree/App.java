package com.sqs.filterfiletree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App 
{
    private String start;
    private String output;
    private String filter;
    private List<String> all;

    public App(String[] args) {
        start = args[0];
        filter = args[1];
        output = args[2];
    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) {
        App app = new App(args);

        try {
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void run() throws IOException {
        all = FileList.create(start);

        filter();

        writeResult();
    }

	void writeResult() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        List<String> all2 = all;
		for (String fs : all2) {
            bw.write(fs);
        }
        bw.close();
	}

	void filter() throws FileNotFoundException, IOException {
		BufferedReader br = createBufferedReader();
        String strLine;
        while ((strLine = br.readLine()) != null) {
            Pattern p = createPattern(strLine);

            List<String> r = new ArrayList<String>();
            for (String fs : all) {
                Matcher m = p.matcher(fs);
                if (m.find()) {
                    r.add(fs);
                }
            }
            all.removeAll(r);
        }
        // TODO: write a test to show that close is not called when an exception occurs 
        // while executing the method 
        br.close();
	}

	Pattern createPattern(String strLine) {
		return Pattern.compile(strLine);
	}

	BufferedReader createBufferedReader() throws FileNotFoundException {
		return new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(filter))));
	}

}
