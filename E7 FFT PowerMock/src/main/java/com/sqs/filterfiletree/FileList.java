package com.sqs.filterfiletree;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileList {

	// TODO: mock this method
	public static List<String> create(String root) throws IOException {
		List<String> all = new ArrayList<String>();

		File s = new File(root);
		all.add(s.getCanonicalPath());
        all.addAll(loop(s));
		
        return all;
	}
	
    private static List<String>  loop(File s) throws IOException {
    	List<String> all = new ArrayList<String>();
        if (s.isDirectory()) {
            File[] a = s.listFiles();
            for (File f : a) {
                if (f.isDirectory()) {
                    loop(f);
                } else {
                    all.add(f.getCanonicalPath());
                }
            }
        }
        return all;
    }

}
