package com.sqs.filterfiletree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App 
{
    private String start;
    private String output;
    private String filter;
    private List<String> all;

    public App(String[] args) {
        start = args[0];
        filter = args[1];
        output = args[2];

        all = new ArrayList<String>();
    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) {
        App app = new App(args);

        try {
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void run() throws IOException {
        File s = new File(start);
        all.add(s.getCanonicalPath());
        loop(s);

        filter();

        BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        for (String fs : all) {
            bw.write(fs);
        }
        bw.close();
    }

	void filter() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(filter))));
        filter(br);
	}

	void filter(BufferedReader br) throws IOException {
		String strLine;
        while ((strLine = br.readLine()) != null) {
            Pattern p = Pattern.compile(strLine);

            List<String> r = new ArrayList<String>();
            for (String fs : all) {
                Matcher m = p.matcher(fs);
                if (m.find()) {
                    r.add(fs);
                }
            }
            all.removeAll(r);
        }
        if (all.isEmpty()) return;
        
        // close isn't called if all is empty
        br.close();
	}

    private void loop(File s) throws IOException {
        if (s.isDirectory()) {
            File[] a = s.listFiles();
            for (File f : a) {
                if (f.isDirectory()) {
                    loop(f);
                } else {
                    all.add(f.getCanonicalPath());
                }
            }
        }
    }
}
