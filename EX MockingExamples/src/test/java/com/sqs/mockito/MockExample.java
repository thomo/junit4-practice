package com.sqs.mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.hamcrest.CoreMatchers.instanceOf;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.sqs.code.MyClassWithStaticMethod;
import com.sqs.code.Wrapper;

public class MockExample {

	private Wrapper wrapper;
	private Wrapper wrapperWithMock;
	private MyClassWithStaticMethod mock;

	@Before
	public void setup() {
		wrapper =  new Wrapper(new MyClassWithStaticMethod());

		mock = Mockito.mock(MyClassWithStaticMethod.class);
		wrapperWithMock = new Wrapper(mock);
	}

	@Ignore
	@Test
	public void shouldExecuteSubMethod() {
		assertEquals("foo:42", wrapper.runAndCallExecuteSomething());
	}

	@Test
	public void shouldNotExecuteSubMethod() {
		assertEquals("foo:0", wrapperWithMock.runAndCallExecuteSomething());
	}

	@Ignore
	@Test
	public void shouldExecuteVoidMethod() {
		assertEquals("bar", wrapper.runAndCallVoid());
	}

	@Test
	public void shouldNotExecuteVoidMethod() {
		assertEquals("bar", wrapperWithMock.runAndCallVoid());
		verify(mock).voidMethod();
	}

	@Test
	public void shouldThrowAnException() {
		Mockito.doThrow(new IndexOutOfBoundsException("foo")).when(mock).voidMethod();
		Exception expectedException = null;
		
		try{
			wrapperWithMock.runAndCallVoid();
			fail("should through an exception");
		} catch (Exception e) {
			expectedException = e;
		}
		
		assertThat(expectedException, instanceOf(IndexOutOfBoundsException.class));
	}
}
