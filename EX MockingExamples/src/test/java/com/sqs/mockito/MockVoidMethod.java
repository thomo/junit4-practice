package com.sqs.mockito;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import org.junit.Test;

class Listener {
	public void action() {
		System.out.println("bla");
	}
}

class World{

    private Listener listener;

	void addListener(Listener item) {
        listener = item;
    }
    
    void action() {
    	listener.action();
    }
    
} 

public class MockVoidMethod {

	@Test
	public void demonstrateDoNothing() {
		Listener lst = mock(Listener.class);
		doNothing().doThrow(new RuntimeException()).when(lst).action();

		World w = new World();
	    w.addListener(lst);
	    
	    w.action();
	    
	    try {
	    	w.action();
	    	fail();
	    } catch (Exception e) {
	    	// pass
	    }
	}
}
