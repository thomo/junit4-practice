package com.sqs.mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;


public class SpyExample {
	@Test
	public void shouldSpy() {
		List<String> list = new LinkedList<String>(); 
		List<String> spy = Mockito.spy(list);
		
		//optionally, you can stub out some methods:
		when(spy.size()).thenReturn(100);
		
		//using the spy calls real methods
		spy.add("one");
		spy.add("two");
		
		//prints "one" - the first element of a list
		System.out.println(spy.get(0));
		
		//size() method was stubbed - 100 is printed
		System.out.println(spy.size());
		
		//optionally, you can verify
		verify(spy).add("one");
		verify(spy).add("two");
	}
}
