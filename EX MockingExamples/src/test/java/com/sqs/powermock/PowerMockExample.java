package com.sqs.powermock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.sqs.code.MyClassWithStaticMethod;


@RunWith(PowerMockRunner.class)
@PrepareForTest( { MyClassWithStaticMethod.class })
public class PowerMockExample {
	private MyClassWithStaticMethod cut;

	@Before
	public void setup() {
		PowerMockito.mockStatic(MyClassWithStaticMethod.class);
		cut = new MyClassWithStaticMethod();
	}
	
	@Test
	public void shouldCalculate42() {
		assertEquals(42, cut.executeSomeThing(21));
	}
	
	@Test
	public void shouldCallShow() {
		cut.executeSomeThing(50);
		
		PowerMockito.verifyStatic();
		MyClassWithStaticMethod.staticShow(any(String.class));
	}
	
	@Test
	public void shouldReturnInjectedValue() {
		Mockito.when(MyClassWithStaticMethod.staticAnswer()).thenReturn(42);
		
		assertEquals(42, MyClassWithStaticMethod.staticAnswer());
	}
}
