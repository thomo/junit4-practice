package com.sqs.powermock;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;

import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

class App {
	public App(String[] strings) {
	}

	public Pattern createPattern(String pattern) {
		return Pattern.compile(pattern);
	}
}

@RunWith(PowerMockRunner.class)
@PrepareForTest( { App.class })
public class PowerMockWithSystemClass {
	@Test
	public void mockStaticSystemMethod() {
		App app = new App(new String[] {"a", "b", "c"});
		
		PowerMockito.mockStatic(Pattern.class);
		Mockito.when(Pattern.compile(Mockito.anyString())).thenThrow(new RuntimeException("foo"));
		
		Exception expectedException = null;
		try {
			app.createPattern("a");
			fail("no exception thrown");
		} catch (Exception e) {
			expectedException  = e;
		}
		
		assertThat(expectedException, instanceOf(RuntimeException.class));
		assertThat(expectedException.getMessage(), containsString("foo"));
		assertThat(expectedException.getMessage(), endsWith("o"));
	}
}
