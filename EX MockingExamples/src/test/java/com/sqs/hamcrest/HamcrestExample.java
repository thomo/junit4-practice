package com.sqs.hamcrest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class HamcrestExample {
	@Test
	public void hamcrest_StartsWith() {
		assertThat("Hello World", startsWith("Hello"));
	}
	
	@Test
	public void hamcrest_Is() {
		assertThat(3, is(equalTo(3)));
	}

	@Test
	public void hamcrest_hasItem() {
		List<String> col = new ArrayList<String>();
		col.add("abc"); col.add("bar"); col.add("foo");
		
		assertThat(col, hasItem("abc"));
		assertThat(col, hasItems("bar", "abc", "foo"));
	}
}
