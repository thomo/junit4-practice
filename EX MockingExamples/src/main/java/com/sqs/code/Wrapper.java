package com.sqs.code;

public class Wrapper {

	private MyClassWithStaticMethod sub;

	public Wrapper(MyClassWithStaticMethod delegate) {
		sub = delegate;
	}

	public String runAndCallExecuteSomething() {
		return "foo:" + sub.executeSomeThing(21);
	}
	
	public String runAndCallVoid() {
		sub.voidMethod();
		return "bar";
	}
	


}
