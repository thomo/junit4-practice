package com.sqs.code;

import javax.swing.JOptionPane;

public class MyClassWithStaticMethod {
	public static void staticShow(String msg) {
		JOptionPane.showMessageDialog(null, msg, "Message", JOptionPane.INFORMATION_MESSAGE);
	}

	public int executeSomeThing(int x) {
		staticShow("done");
		return x*2;
	}
	
	public static int staticAnswer() {
		return 21;
	}
	
	public void voidMethod() {
		staticShow("void");
	}
}